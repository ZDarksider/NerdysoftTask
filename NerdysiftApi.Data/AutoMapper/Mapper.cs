﻿using AutoMapper;
using NerdysiftApi.Data.DTO;
using NerdysiftApi.Data.DTO.HelperModels;
using NerdysoftApi.Buisiness.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NerdysiftApi.Data.AutoMapper
{
    public class Mapper
    {
        public class ObjectMapper
        {
            private static readonly Lazy<IMapper> Lazy = new Lazy<IMapper>(() =>
            {
                var config = new MapperConfiguration(cfg =>
                {
                    // This line ensures that internal properties are also mapped over.
                    cfg.ShouldMapProperty = p => p.GetMethod.IsPublic || p.GetMethod.IsAssembly;
                    cfg.AddProfile<NerdysoftApiMapper>();
                });
                var mapper = config.CreateMapper();
                return mapper;
            });

            public static IMapper Mapper => Lazy.Value;
        }
    }

    public class NerdysoftApiMapper : Profile
    { 
        public NerdysoftApiMapper()
        {
            CreateMap<Announcement, AnnouncementModel>().ReverseMap();
            CreateMap<Announcement, AnnoncementComparisonModel>().ReverseMap();
            CreateMap<Announcement, AnnouncementDetailedModel>().ReverseMap();
            CreateMap<AnnouncementModel, AnnoncementComparisonModel>().ReverseMap();
        }
    }
    }
