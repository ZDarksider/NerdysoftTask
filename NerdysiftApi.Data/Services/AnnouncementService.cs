﻿using NerdysiftApi.Data.AutoMapper;
using NerdysiftApi.Data.DTO;
using NerdysiftApi.Data.DTO.HelperModels;
using NerdysiftApi.Data.DTO.Request_Models;
using NerdysiftApi.Data.Services.Service_Interfaces;
using NerdysoftApi.Buisiness.Data;
using NerdysoftApi.Buisiness.Models;
using NerdysoftApi.Buisiness.Repository;
using NerdysoftApi.Buisiness.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static NerdysiftApi.Data.AutoMapper.Mapper;

namespace NerdysiftApi.Data.Services
{
    public class AnnouncementService : IAnnouncementService
    {

        private readonly IAnnouncementRepository repository;
        private readonly IDateTimeProvider dateTimeProvider;

        public AnnouncementService(IAnnouncementRepository repository, IDateTimeProvider dateTimeProvider)
        {
            this.repository = repository;
            this.dateTimeProvider = dateTimeProvider;
        }

        public async Task<AnnouncementModel> AddNewAnnouncementAsync(AnnouncementRequest request)
        {
            if (request.Description is null || request.Title is null)
            {
                throw new ArgumentException("Invalid Request Data");
            }
            var dateNow = dateTimeProvider.Now();
            var newAnnouncement = new Announcement() { Title = request.Title, Description = request.Description, DateAdded =dateNow };
            await repository.CreateAnnouncementAsync(newAnnouncement);
            return ObjectMapper.Mapper.Map<AnnouncementModel>(newAnnouncement);
        }

        public async Task DeleteAnnouncementAsync(int id)
        {
            var announcement = await repository.GetAnnouncementByIdAsync(id);
            if (announcement is null)
            {
                throw new ArgumentException("invalid ID");
            }
            await repository.DeleteAnnouncementAsync(announcement);
        }

        public async Task<AnnouncementModel> EditAnnouncementAsync(EditAnnouncementRequest request)
        {
            var announcement = await repository.GetAnnouncementByIdAsync(request.Id);
            if (request.Description is null || request.Title is null || announcement is null)
            {
                throw new ArgumentException("Invalid Request Data");
            }

            announcement.Description = request.Description;
            announcement.Title = request.Title;
            await repository.EditAnnouncementAsync(announcement);

            return ObjectMapper.Mapper.Map<AnnouncementModel>(announcement);
        }

        public async Task<IEnumerable<AnnouncementModel>> GetAllAnnouncementAsync()
        {
            var announcements = await repository.GetAllAnnouncementsAsync();
            return ObjectMapper.Mapper.Map<IEnumerable<AnnouncementModel>>(announcements);
        }

        public async  Task<AnnouncementDetailedModel> GetAnnouncementDetails(int id)
        {
            var announcementToDetail = await repository.GetAnnouncementByIdAsync(id);
            if (announcementToDetail is null)
            {
                throw new ArgumentException("invalid Id");
            }
            var allAnnouncements = await repository.GetAllAnnouncementsAsync();
            List<AnnoncementComparisonModel> annoncementComparisonModels = new();
            foreach (var announcement in allAnnouncements)
            {
                var comparisonAnnouncement = ObjectMapper.Mapper.Map<AnnoncementComparisonModel>(announcement);
                comparisonAnnouncement.Descriptiowords = announcement.Description.Split(" ").ToList();
                comparisonAnnouncement.TitleWords = announcement.Title.Split(" ").ToList();
                annoncementComparisonModels.Add(comparisonAnnouncement);
            }
            var targetAnnouncementModel = annoncementComparisonModels.Where(x => x.AnnouncementId == announcementToDetail.AnnouncementId).FirstOrDefault();
            var comparisonModelToWorkWith = annoncementComparisonModels.Where(x => x.AnnouncementId != announcementToDetail.AnnouncementId).ToList();

            var simmilarAnnouncement = comparisonModelToWorkWith
                .Where(x => x.Descriptiowords.Where(y => targetAnnouncementModel.Descriptiowords.Contains(y)).Any() 
                && x.TitleWords.Where(y => targetAnnouncementModel.TitleWords.Contains(y)).Any() ).Take(3).ToList();
            var mapperdSimilarAnnouncement = ObjectMapper.Mapper.Map<IEnumerable<AnnouncementModel>>(simmilarAnnouncement);
            var finalModel = ObjectMapper.Mapper.Map<AnnouncementDetailedModel>(announcementToDetail);
            finalModel.SimmilarAnnouncements = mapperdSimilarAnnouncement.ToList();
            return finalModel;
        }
    }
}
