﻿using NerdysiftApi.Data.DTO;
using NerdysiftApi.Data.DTO.Request_Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NerdysiftApi.Data.Services.Service_Interfaces
{
    public interface IAnnouncementService
    {
        public Task<AnnouncementModel> AddNewAnnouncementAsync(AnnouncementRequest request);

        public Task<AnnouncementModel> EditAnnouncementAsync(EditAnnouncementRequest request);

        public Task DeleteAnnouncementAsync(int id);
        public Task<IEnumerable<AnnouncementModel>> GetAllAnnouncementAsync();
        public Task<AnnouncementDetailedModel> GetAnnouncementDetails(int id);

    }
}
