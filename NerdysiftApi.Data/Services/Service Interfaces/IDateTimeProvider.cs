﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NerdysiftApi.Data.Services.Service_Interfaces
{
    public interface IDateTimeProvider
    {
        public DateTime Now();
    }
}
