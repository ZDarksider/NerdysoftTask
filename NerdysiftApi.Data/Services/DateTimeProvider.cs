﻿using NerdysiftApi.Data.Services.Service_Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NerdysiftApi.Data.Services
{
    public class DateTimeProvider : IDateTimeProvider
    {
        
        public DateTime Now()
        {
            return DateTime.Now;
        }
    }
}
