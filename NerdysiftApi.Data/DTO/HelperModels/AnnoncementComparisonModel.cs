﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NerdysiftApi.Data.DTO.HelperModels
{
    public class AnnoncementComparisonModel
    {
        public int AnnouncementId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DateAdded { get; set; }

        public List<string> TitleWords { get; set; }
        public List<string> Descriptiowords { get; set; }
    }
}
