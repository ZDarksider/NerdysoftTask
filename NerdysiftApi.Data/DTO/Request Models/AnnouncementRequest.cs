﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NerdysiftApi.Data.DTO.Request_Models
{
    public class AnnouncementRequest
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
