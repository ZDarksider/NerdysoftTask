﻿using Microsoft.EntityFrameworkCore;
using NerdysoftApi.Buisiness.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NerdysoftApi.Buisiness.Data
{
    public class NerdysoftApiContext :DbContext
    {
        public NerdysoftApiContext(DbContextOptions<NerdysoftApiContext> options) : base(options)
        {

        }

        public DbSet<Announcement> Announcements { get; set; }
    }
}
