﻿using NerdysoftApi.Buisiness.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NerdysoftApi.Buisiness.Repository.Base
{
    public interface IAnnouncementRepository
    {
        public Task<IEnumerable<Announcement>> GetAllAnnouncementsAsync();
        public Task<Announcement> GetAnnouncementByIdAsync(int id);
        public Task DeleteAnnouncementAsync(Announcement announcement);
        public Task CreateAnnouncementAsync(Announcement announcement);
        public Task EditAnnouncementAsync(Announcement announcement);
    }
}
