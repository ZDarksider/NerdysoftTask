﻿using Microsoft.EntityFrameworkCore;
using NerdysoftApi.Buisiness.Data;
using NerdysoftApi.Buisiness.Models;
using NerdysoftApi.Buisiness.Repository.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NerdysoftApi.Buisiness.Repository
{
    public class AnnouncementRepository : IAnnouncementRepository
    {
        public NerdysoftApiContext context;

        public AnnouncementRepository(NerdysoftApiContext context)
        {
            this.context = context;
        }
        public async Task CreateAnnouncementAsync(Announcement announcement)
        {
            context.Announcements.Add(announcement);
            await context.SaveChangesAsync();
        }

        public async Task DeleteAnnouncementAsync(Announcement announcement)
        {
            context.Announcements.Remove(announcement);
            await context.SaveChangesAsync();
        }

        public async Task EditAnnouncementAsync(Announcement announcement)
        {
            context.Announcements.Update(announcement);
            await context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Announcement>> GetAllAnnouncementsAsync()
        {
           return await  context.Announcements.ToListAsync();
        }

        public async Task<Announcement> GetAnnouncementByIdAsync(int id)
        {
            return await context.Announcements.Where(x => x.AnnouncementId.Equals(id)).FirstOrDefaultAsync();
        }
    }
}
