﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NerdysiftApi.Data.DTO.Request_Models;
using NerdysiftApi.Data.Services.Service_Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NerdysoftApi.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnnouncementController : ControllerBase
    {
        public readonly IAnnouncementService announcementService;

        public AnnouncementController(IAnnouncementService announcementService)
        {
            this.announcementService = announcementService;
        }

        [HttpGet]
        [Route("GetAllAnnouncements")]
        public async Task<IActionResult> GetAllAnnouncements()
        {
            var announcements = await announcementService.GetAllAnnouncementAsync();
            return Ok(announcements);
        }

        [HttpGet]
        [Route("GetAnnoncementDetails")]
        public async Task<IActionResult> GetAnnoncementDetails(int id)
        {
            var announcement = await announcementService.GetAnnouncementDetails(id);
            return Ok(announcement);
        }
        [HttpPost]
        [Route("CreateAnnoncement")]
        public async Task<IActionResult> CreateAnnouncement(AnnouncementRequest request)
        {
            var announcement = await announcementService.AddNewAnnouncementAsync(request);
            return Ok(announcement);
        }
        [HttpPut]
        [Route("EditAnnouncement")]
        public async Task<IActionResult> EditAnnouncement(EditAnnouncementRequest request)
        {
            var announcement = await announcementService.EditAnnouncementAsync(request);
            return Ok(announcement);
        }
        [HttpDelete]
        [Route("DeleteAnnouncement")]
        public async Task<IActionResult> DeleteAnnouncement(int id)
        {
            await announcementService.DeleteAnnouncementAsync(id);
            return NoContent();
        }
    }
}
